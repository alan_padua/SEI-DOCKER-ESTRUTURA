#!/bin/bash

set -e

## verifico se base já existe (volume persistente)
DBNAME="sip"
DBEXISTS=$(mysql --batch --skip-column-names -e "SHOW DATABASES LIKE '"$DBNAME"';" | grep "$DBNAME" > /dev/null; echo "$?")

if [ $DBEXISTS -eq 0 ];then
	echo "A database with the name $DBNAME already exists."
else
	echo " database $DBNAME does not exist. Creating..."
	mysql -uroot -proot -e "create database sei"; 
	mysql -uroot -proot -e "create database sip"; 

	# Criação dos usuários utilizados na conexão com SEI e SIP
	mysql -uroot -proot -e "CREATE USER 'sip_user'@'%' IDENTIFIED BY 'sip_user'" sip
	mysql -uroot -proot -e "CREATE USER 'sei_user'@'%' IDENTIFIED BY 'sei_user'" sei
	mysql -uroot -proot -e "GRANT ALL PRIVILEGES ON sip.* TO 'sip_user'@'%'" sip
	mysql -uroot -proot -e "GRANT ALL PRIVILEGES ON sei.* TO 'sei_user'@'%'" sei

	# Restauração dos bancos de dados
	mysql -uroot -proot sei < /tmp/sei_3_1_0_BD_Ref_Exec.sql
	mysql -uroot -proot sip < /tmp/sip_3_1_0_BD_Ref_Exec.sql

	# Atualização dos parâmetros do SEI e do SIP
	mysql -uroot -proot -e "update orgao set sigla='ORGAO', descricao='ORGAO ORGAO' where id_orgao=0;" sip
	mysql -uroot -proot -e "update sistema set pagina_inicial='http://localhost/sip' where sigla='SIP';" sip
	mysql -uroot -proot -e "update sistema set pagina_inicial='http://localhost/sei/inicializar.php', web_service='http://localhost/sei/controlador_ws.php?servico=sip' where sigla='SEI';" sip
	mysql -uroot -proot -e "update orgao set sigla='ORGAO', descricao='ORGAO ORGAO' where id_orgao=0;" sei

	# Remove registros de auditoria presentes na base de referência
	mysql -uroot -proot -e "delete from auditoria_protocolo;" sei

	# Configuração para utilizar autenticação nativa do SEI/SIP
	mysql -uroot -proot -e "update orgao set sin_autenticar='N' where id_orgao=0;" sip

	rm -rf /tmp/*

fi

# Atribuição de permissões de acesso externo para o usuário root, senha root
#mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION;"

exit 0
