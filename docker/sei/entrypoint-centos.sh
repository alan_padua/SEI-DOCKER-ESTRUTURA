#!/bin/bash

# ENVIRONMENT VARIABLES
# $SEI_HOST_URL
# $SEI_ORGAO
# $SEI_DB_USERNAME
# $SEI_DB_PASSWORD
# $SMTP_SERVER
# $SMTP_PORT

    set -e
        
    # Direciona logs para saida padrão para utilizar docker logs
    ln -sf /dev/stdout /var/log/httpd/access_log
    ln -sf /dev/stdout /var/log/httpd/ssl_access_log
    ln -sf /dev/stdout /var/log/httpd/ssl_request_log
    ln -sf /dev/stderr /var/log/httpd/error_log
    ln -sf /dev/stderr /var/log/httpd/ssl_error_log


    # Set etc/hosts
    # if [ -z "$SEI_PROD" ]; then
        #IP=`ifconfig | awk '/inet addr/{print substr($2,6)}' | head -1`
        IP=`hostname -I | awk '{print $1}'` 
    
        echo $IP $SEI_HOST_URL >> /etc/hosts 
    # fi
    # /usr/sbin/apache2ctl -D FOREGROUND
exec "$@"
