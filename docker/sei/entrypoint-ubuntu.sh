#!/bin/bash

# ENVIRONMENT VARIABLES
# $SEI_HOST_URL
# $SEI_ORGAO
# $SEI_DB_USERNAME
# $SEI_DB_PASSWORD
# $SMTP_SERVER
# $SMTP_PORT

    set -e
        
    # Direciona logs para saida padrão para utilizar docker logs

    # Set etc/hosts
    # if [ -z "$SEI_PROD" ]; then
        IP=`hostname -I | awk '{print $1}'` 
        
        echo $IP $SEI_HOST_URL >> /etc/hosts 
    # fi
    # /usr/sbin/apache2ctl -D FOREGROUND
exec "$@"
