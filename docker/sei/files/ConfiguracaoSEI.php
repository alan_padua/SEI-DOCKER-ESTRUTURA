<?

class ConfiguracaoSEI extends InfraConfiguracao  {

 	private static $instance = null;

 	public static function getInstance(){
 	  if (ConfiguracaoSEI::$instance == null) {
 	    ConfiguracaoSEI::$instance = new ConfiguracaoSEI();
 	  }
 	  return ConfiguracaoSEI::$instance;
 	}

 	public function getArrConfiguracoes(){				
		// $strValAmbiente = getenv('SEI_MODULOS');
		
		// if($strValAmbiente !== ""){
		// 	$ArrayModulos = null;
		// 	$strValAmbienteTratada = str_replace("	", "", str_replace("\n","",str_replace("'","",str_replace('"',"",$strValAmbiente))));
			
		// 	$arrMod = explode(",",$strValAmbienteTratada);
		// 	foreach($arrMod as $Mod){
		// 		$arrTemp = explode("=>",$Mod);
		// 		$chave = trim($arrTemp[0]);
		// 		$valor = trim($arrTemp[1]);
		// 		$ArrayModulos[$chave] = $valor;
		// 	}
		// }
		
 	  return array(

 	      'SEI' => array(
 	          'URL' => 'http://'.getenv('SEI_HOST_URL').'/sei',
			  'Producao' => true,
			  'RepositorioArquivos' => '/opt/dados'),
 	        //   'RepositorioArquivos' => '/dados',
            //       'Modulos' => $ArrayModulos
            //   ),

 	      'PaginaSEI' => array(
 	          'NomeSistema' => 'SEI',
 	          'NomeSistemaComplemento' => getenv('SEI_NOMECOMPLEMENTO'),
              'LogoMenu' => ''),

 	      'SessaoSEI' => array(
 	          'SiglaOrgaoSistema' => getenv('SEI_ORGAO'),
 	          'SiglaSistema' => 'SEI',
 	          'PaginaLogin' => 'http://'.getenv('SEI_HOST_URL').'/sip/login.php',
 	          'SipWsdl' => 'http://'.getenv('SEI_HOST_URL').'/sip/controlador_ws.php?servico=wsdl',
 	          'https' => false),
 	      
 	    //   'XSS' => array('NivelVerificacao' => 'A', //B=Básico, A=Avançado, N=Nenhum
        //        'ProtocolosExcecoes' => array(), //lista temporária de documentos nao validados
        //        'NivelBasico' => array('ValoresNaoPermitidos' => null),
        //        'NivelAvancado' => array('TagsPermitidas' => null,
        //                                 'TagsAtributosPermitidos' => null,
        //                                 'FiltrarConteudoConsulta' => false //neste caso loga os erros e filtra o conteudo nas consultas
        //                                 )
        //                 ),
 	       
 	      'BancoSEI'  => array(
                  'Servidor' => getenv('HOST_MYSQL'),
                  'Porta' => getenv('SEI_DB_PORT'),
                  'Banco' => getenv('SEI_DB_DATABASENAME'),
                  'Usuario' => getenv('SEI_DB_USERNAME'),
                  'Senha' => getenv('SEI_DB_PASSWORD'),
                  'UsuarioScript' => getenv('SEI_DB_USERNAME'),
                  'SenhaScript' => getenv('SEI_DB_PASSWORD'),
                  'Tipo' => 'MySql'), //MySql ou SqlServer

              'CacheSEI' => array('Servidor' => getenv('HOST_MEMCACHE'),'Porta' => '11211'),

              'JODConverter' => array('Servidor' => 'http://'.getenv('HOST_JODCONVERTER').':8080/converter/service'),

 	      'Edoc' => array('Servidor' => 'http://[Servidor .NET]'),
 	       
 	      'Solr' => array(
                  'Servidor' => 'http://'.getenv('HOST_SOLR').':8983/solr',
                  'CoreProtocolos' => 'sei-protocolos',
                  'TempoCommitProtocolos' => 300,
                  'CoreBasesConhecimento' => 'sei-bases-conhecimento',
                  'TempoCommitBasesConhecimento' => 60,
                  'CorePublicacoes' => 'sei-publicacoes',
                  'TempoCommitPublicacoes' => 60),

	      'HostWebService' => array(
	          'Edoc' => array('*'),
		  'Sip' => array('*'), 
		  'Publicacao' => array('*'), 
		  'Ouvidoria' => array('*'),),
 	       
 	      'InfraMail' => array(
	   	  'Tipo' => getenv('EMAIL_TIPO'),  //1 = sendmail (neste caso n�o � necess�rio configurar os atributos abaixo), 2 = SMTP
		  'Servidor' =>  getenv('SERVIDOR_EMAIL'),
		  'Porta' => '25',
		  'Codificacao' => '8bit', 
		  'MaxDestinatarios' => 999, 
		  'MaxTamAnexosMb' => 999, 
		  'Seguranca' => getenv('EMAIL_SEGURANCA'), 
		  'Autenticar' => getenv('EMAIL_AUTENTICAR'), 
		  'Usuario' => getenv('EMAIL_USUARIO'),
		  'Senha' => getenv('EMAIL_SENHA'),
          'MaxTamAnexosMb' => 20,
		  'Protegido' => '')
 	  );
 	}
}
?>