<?

class ConfiguracaoSip extends InfraConfiguracao  {

 	private static $instance = null;

 	public static function getInstance(){
 	  if (ConfiguracaoSip::$instance == null) {
 	    ConfiguracaoSip::$instance = new ConfiguracaoSip();
 	  }
 	  return ConfiguracaoSip::$instance;
 	}

 	public function getArrConfiguracoes(){
 	  return array(
 	      'Sip' => array(
 	          'URL' => 'http://'.getenv('SEI_HOST_URL').'/sip',
 	          'Producao' => true,
			  'Modulos' => array()
			  ),
 	       
 	      'PaginaSip' => array('NomeSistema' => 'SIP'),

 	      'SessaoSip' => array(
 	          'SiglaOrgaoSistema' => getenv('SEI_ORGAO'),
 	          'SiglaSistema' => 'SIP',
 	          'PaginaLogin' => 'http://'.getenv('SEI_HOST_URL').'/sip/login.php',
 	          'SipWsdl' => 'http://'.getenv('SEI_HOST_URL').'/sip/controlador_ws.php?servico=wsdl',
 	          'https' => false),
 	       
 	      'BancoSip'  => array(
 	          'Servidor' => getenv('HOST_MYSQL'),
              'Porta' => getenv('SIP_DB_PORT'),
              'Banco' => getenv('SIP_DB_DATABASENAME'),
 	          'Usuario' => getenv('SIP_DB_USERNAME'),
 	          'Senha' => getenv('SIP_DB_PASSWORD'),
 	          'UsuarioScript' => getenv('SIP_DB_USERNAME'),
              'SenhaScript' => getenv('SIP_DB_PASSWORD'),
 	          'Tipo' => 'MySql'), //MySql, SqlServer ou Oracle

              'CacheSip' => array('Servidor' => getenv('HOST_MEMCACHE'), 'Porta' => '11211'),
 	       
 	      'HostWebService' => array(
 	          'Replicacao' => array(''), //endereo ou IP da mquina que implementa o servio de replicao de usurios
 	          'Pesquisa' => array('*'), //endereos/IPs das mquinas do SEI
 	          'Autenticacao' => array('*')), //endereos/IPs das mquinas do SEI
 	      
 	      'InfraMail' => array(
 	          'Tipo' => '1', //1 = sendmail (neste caso no  necessrio configurar os atributos abaixo), 2 = SMTP
 	          'Servidor' => getenv('SERVIDOR_EMAIL'),
 	          'Porta' => '25',
 	          'Seguranca' => getenv('EMAIL_SEGURANCA'),
 	          'Codificacao' => '8bit', //8bit, 7bit, binary, base64, quoted-printable
 	          'Autenticar' => getenv('EMAIL_AUTENTICAR'), //se true ento informar Usuario e Senha
 	          'Usuario' => getenv('EMAIL_USUARIO'),
 	          'Senha' => getenv('EMAIL_SENHA'),
                  'MaxTamAnexosMb' => 20,
 	          'Protegido' => ''),  //campo usado em desenvolvimento, se tiver um email preenchido entao todos os emails enviados terao o destinatario ignorado e substitudo por este valor (evita envio incorreto de email)

 	  );
 	  	
 	}
}
?>
