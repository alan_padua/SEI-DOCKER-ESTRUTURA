# sei

## 1- Para subir a aplicação em ambienten **LOCAL**

### 1 - Passo
Entrar na pasta **docker**

### 2- Passo 
Executar o comando:

    docker-compose up --build

Portas de  saida das aplicações:

	sei: [HOST]:80
    jodconverter: [HOST]8080
    solr: [HOST]:8983
    mysql: [HOST]3306

## 2- Para derrubar/remover imagens

### 1- Passo

Executar o comando:

     docker-compose down

### 2- Passo

Para remover as imagens na máquina local:

Executar o comando:

    docker-compose rm


## 3 - Para publicar no registry do orgao:

Publicar no registry do orgao

### 1 - Passo
Logar no registry do orgao:
    - docker login -u 'ususario' -p 'senha' https://${REGISTRY_ORGAO}

### 2 - Passo
Publicar uma única imagem:

    - docker build  -t ${REGISTRY_ORGAO}/sei-app/sei_docker:1.0.0 .

### 3 - Passo
Publicar todas as imagens:

    - docker-compose push


## 4- Variaveis de ambiente:

Estão no arquivo: docker/docker-compose.yml

SEI:
- SEI_HOST_URL=localhost
- SEI_ORGAO=ORGAO
- SEI_DB_DATABASENAME=sei
- SEI_DB_PASSWORD=root
- SEI_DB_USERNAME=root
- SEI_DB_PORT=3306
- SIP_DB_DATABASENAME=sip
- SIP_DB_PASSWORD=root
- SIP_DB_USERNAME=root
- SIP_DB_PORT=3306
- SERVIDOR_EMAIL=10.10.0.234
- SEI_NOMECOMPLEMENTO=ORGAO
- STDOUT=/var/log/apache2/error.log
- EMAIL_SEGURANCA=TSL
- EMAIL_AUTENTICAR=false
- EMAIL_TIPO=1
- SEI_PROD=false
- HOST_MYSQL=mysql
- HOST_JODCONVERTER=jodconverter
- HOST_MEMCACHE=memcache
- HOST_SOLR=sei-solr
- EMAIL_USUARIO=
- EMAIL_SENHA=


MYSQL:
- MYSQL_ROOT_PASSWORD=root
- MYSQL_USER=adim
- MYSQL_PASSWORD=admin

OBS: No arquivo **.env ** estão as variaveis referente a execução do docker-compose, preferi deixar separado para não haver confusão

### 4. 1 - Sistema Operacional:

Para selecionar qual o **Sistema Operacional** que será usado deve ser definido no arquivo **.env **.

CENTOS: 
VERSAO_SO=Dockerfile_CENTOS

UBUNTU:
VERSAO_SO=Dockerfile_UBUNTU

## 5- Links entre aplicações

Para que as aplicações consigam conversar entre si, criei links para não haver a necessidade setar ip entre elas.

Links:
- mysql:mysql
- jodconverter:jod
- memcache:memcache
- sei-solr:sei-solr

## 6- Script de Banco de Dados

### 1 - Passo

A criação é feita **automaticamente** pelo script dentro co container mysql.

Script de inicialização do banco: 

	docker/mysql/install.sh

Scritpts iniciais:

- sei_3_1_0_BD_Ref_Exec.sql
- sip_3_1_0_BD_Ref_Exec.sql

### 2 - Passo

Após a primeira execução é necessário atualizar as tabelas abaixo.

	update sistema set pagina_inicial='http://[servidor_sip]/sip' where sigla='SIP';

	update sistema set 	pagina_inicial='http://[servidor_sei]/sei/inicializar.php',  web_service='http://[servidor_sei]/sei/controlador_ws.php?servico=sip' where sigla='SEI';

	update sip.orgao set sigla='ORGAO', descricao='ORGAO ORGAO', sin_ativo='S', sin_autenticar='N', ordem=0 where id_orgao=0;



## 7 - Atualiza versão do SEI

Entrar no container do SEI para executar os scripts:

Pasta /opt/sei/scripts:

    php -c /etc/sei.ini /opt/sei/scripts/atualizar_versao_sei.log     
    php -c /etc/sei.ini /opt/sei/scripts/atualizar_versao.php  
    php -c /etc/sei.ini /opt/sei/scripts/atualizar_sequencias.php 

    php -c /etc/sei.ini /opt/sei/scripts/AgendamentoTarefaSEI.php              
    php -c /etc/sei.ini /opt/sei/scripts/indexacao_bases_conhecimento.php     
    php -c /etc/sei.ini /opt/sei/scripts/indexacao_publicacoes.php
    php -c /etc/sei.ini /opt/sei/scripts/indexacao_controle_interno.php        
    php -c /etc/sei.ini /opt/sei/scripts/indexacao_parcial.php                 
    php -c /etc/sei.ini /opt/sei/scripts/indexacao_processo.php               
    php -c /etc/sei.ini /opt/sei/scripts/indexacao_protocolos_completa.php  
    
    php -c /etc/sei.ini /opt/sei/scripts/migracao_edoc.php
    php -c /etc/sei.ini /opt/sei/scripts/verificacao_repositorio_arquivos.php        
    php -c /etc/sei.ini /opt/sei/scripts/verificacao_xss.php
    php -c /etc/sei.ini /opt/sei/scripts/importacao_pctt_abril_2016.php    

Pasta /opt/sei/scripts:
    php -c /etc/sei.ini /opt/sip/scripts/AgendamentoTarefaSip.php  
    php -c /etc/sei.ini /opt/sip/scripts/atualizar_sequencias.php  
    php -c /etc/sei.ini /opt/sip/scripts/atualizar_versao_sei.php

## 7 - Atualizar Indices do Solr

http://[servidor_solr]:8983/solr/admin/cores?action=CREATE&name=sei-protocolos&instanceDir=/dados/sei-protocolos&config=sei-protocolos-config.xml&schema=sei-protocolos-schema.xml&dataDir=/dados/sei-protocolos/conteudo

http://[servidor_solr]:8983/solr/admin/cores?action=CREATE&name=sei-bases-conhecimento&instanceDir=/dados/sei-bases-conhecimento&config=sei-bases-conhecimento-config.xml&schema=sei-bases-conhecimento-schema.xml&dataDir=/dados/sei-bases-conhecimento/conteudo

http://[servidor_solr]:8983/solr/admin/cores?action=CREATE&name=sei-publicacoes&instanceDir=/dados/sei-publicacoes&config=sei-publicacoes-config.xml&schema=sei-publicacoes-schema.xml&dataDir=/dados/sei-publicacoes/conteudo

## 8 - Manuais

- https://softwarepublico.gov.br/social/sei/manuais/manual-de-instalacao/
- https://github.com/spbgovbr/sei-db-ref-executivo/blob/master/mysql/v3.1.0/sip_3_1_0_BD_Ref_Exec.sql
- https://github.com/spbgovbr



## 9 - Baixar no Windows

### Solução do ERRO: Docker for Windows: Dealing With Windows Line Endings

Esse erro ocorre somente ao baixar no ambiente windows

Deve ser forçado o comando **--config core.autocrlf=input** ao baixar do git


    git clone http://gitlab.ORGAO.com.br/g-sei/sei.git sei_windows --config core.autocrlf=input